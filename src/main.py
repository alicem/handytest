
import sys
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '0.0')

from gi.repository import Gtk, Gio, GObject, Handy

from .window import HandytestWindow

GObject.type_register(Handy.Column)

class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.gnome.Handytest',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = HandytestWindow(application=self)
        win.present()


def main(version):
    app = Application()
    return app.run(sys.argv)
